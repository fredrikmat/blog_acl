-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2016 at 07:55 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cake_blog_tutorial_acl`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_acos_lft_rght` (`lft`,`rght`),
  KEY `idx_acos_alias` (`alias`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 76),
(2, 1, NULL, NULL, 'Groups', 2, 13),
(3, 2, NULL, NULL, 'index', 3, 4),
(4, 2, NULL, NULL, 'view', 5, 6),
(5, 2, NULL, NULL, 'add', 7, 8),
(6, 2, NULL, NULL, 'edit', 9, 10),
(7, 2, NULL, NULL, 'delete', 11, 12),
(8, 1, NULL, NULL, 'Pages', 14, 17),
(9, 8, NULL, NULL, 'display', 15, 16),
(10, 1, NULL, NULL, 'Posts', 18, 33),
(11, 10, NULL, NULL, 'index', 19, 20),
(12, 10, NULL, NULL, 'view', 21, 22),
(13, 10, NULL, NULL, 'add', 23, 24),
(14, 10, NULL, NULL, 'edit', 25, 26),
(15, 10, NULL, NULL, 'delete', 27, 28),
(16, 1, NULL, NULL, 'Users', 34, 49),
(17, 16, NULL, NULL, 'index', 35, 36),
(18, 16, NULL, NULL, 'login', 37, 38),
(19, 16, NULL, NULL, 'logout', 39, 40),
(20, 16, NULL, NULL, 'view', 41, 42),
(21, 16, NULL, NULL, 'add', 43, 44),
(22, 16, NULL, NULL, 'edit', 45, 46),
(23, 16, NULL, NULL, 'delete', 47, 48),
(24, 1, NULL, NULL, 'Widgets', 50, 61),
(25, 24, NULL, NULL, 'index', 51, 52),
(26, 24, NULL, NULL, 'view', 53, 54),
(27, 24, NULL, NULL, 'add', 55, 56),
(28, 24, NULL, NULL, 'edit', 57, 58),
(29, 24, NULL, NULL, 'delete', 59, 60),
(30, 1, NULL, NULL, 'AclExtras', 62, 63),
(31, 10, NULL, NULL, 'permission_check', 29, 30),
(32, 1, NULL, NULL, 'Comments', 64, 75),
(33, 32, NULL, NULL, 'index', 65, 66),
(34, 32, NULL, NULL, 'view', 67, 68),
(35, 32, NULL, NULL, 'add', 69, 70),
(36, 32, NULL, NULL, 'edit', 71, 72),
(37, 32, NULL, NULL, 'delete', 73, 74),
(38, 10, NULL, NULL, 'isAuthorized', 31, 32);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aros_lft_rght` (`lft`,`rght`),
  KEY `idx_aros_alias` (`alias`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, NULL, 1, 2),
(2, NULL, 'Group', 2, NULL, 3, 4),
(3, NULL, 'Group', 3, NULL, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) COLLATE utf8_swedish_ci NOT NULL DEFAULT '0',
  `_read` varchar(2) COLLATE utf8_swedish_ci NOT NULL DEFAULT '0',
  `_update` varchar(2) COLLATE utf8_swedish_ci NOT NULL DEFAULT '0',
  `_delete` varchar(2) COLLATE utf8_swedish_ci NOT NULL DEFAULT '0',
  `_admin` varchar(2) COLLATE utf8_swedish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`),
  KEY `idx_aco_id` (`aco_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`, `_admin`) VALUES
(1, 1, 1, '1', '1', '1', '1', '1'),
(2, 2, 1, '-1', '-1', '-1', '-1', '-1'),
(3, 2, 10, '1', '1', '1', '1', '1'),
(4, 2, 24, '1', '1', '1', '1', '-1'),
(5, 3, 1, '-1', '-1', '-1', '-1', '-1'),
(6, 3, 13, '1', '1', '1', '1', '1'),
(7, 3, 14, '1', '1', '1', '1', '-1'),
(8, 3, 27, '1', '1', '1', '1', '-1'),
(9, 3, 28, '1', '1', '1', '1', '-1'),
(10, 3, 19, '1', '1', '1', '1', '-1');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `body` text COLLATE utf8_swedish_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `name`, `email`, `body`, `created`, `modified`) VALUES
(1, 11, 'Fredrik', 'fredrik@email.com', 'This is a test comment', '2016-06-13 08:35:56', '2016-06-13 08:35:56'),
(2, 11, 'Kaffemannen', 'drick@kaffe.nu', 'Mer kaffe till folket!', '2016-06-13 08:36:25', '2016-06-13 08:36:25'),
(3, 11, 'Fredrik', 'ocrion@wow.com', 'Gravatar test test 123 http://gravatar.com', '2016-06-13 10:14:25', '2016-06-13 10:49:22'),
(4, 3, 'Test ', 'default@email.com', 'This is a comment', '2016-06-13 10:17:42', '2016-06-13 10:17:42'),
(5, 5, 'Markus', 'markus.hannerz@yahoo.se', 'Det ser ut som att jag kan se detta!', '2016-06-13 11:12:14', '2016-06-13 11:12:14'),
(6, 6, 'Fredrik', 'fredrik@email.com', 'Testing the sanitize &lt;function&gt;\\nThis will &#039;;1=1;\\n\\n&lt;script&gt;alert(&quot;Hello :)&quot;)&lt;/script&gt;', '2016-06-13 11:45:27', '2016-06-13 11:45:27');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'administrators', '2016-06-09 10:17:11', '2016-06-09 10:17:11'),
(2, 'managers', '2016-06-09 10:17:15', '2016-06-09 10:17:15'),
(3, 'users', '2016-06-09 10:17:18', '2016-06-09 10:17:18');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `body` text COLLATE utf8_swedish_ci,
  `photo` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `photo_dir` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `body`, `photo`, `photo_dir`, `created`, `modified`, `published`) VALUES
(1, 1, 'Hello world :)', 'Hai', NULL, '', '2016-06-09 11:52:01', '2016-06-14 14:12:36', 0),
(2, 3, 'Hello world once again', 'Greetings!', NULL, '', '2016-06-09 13:03:10', '2016-06-15 15:19:12', 0),
(3, 1, 'Hello world, 2.0', 'Upgrade to 2.0 world hello...', NULL, NULL, '2016-06-09 13:03:52', '2016-06-10 16:52:25', 1),
(5, 2, 'Test preview', 'Testing the preview function', NULL, NULL, '2016-06-10 09:48:22', '2016-06-14 09:31:10', 1),
(6, 2, 'Draft post', 'This is a drafted post, only administrators are able to see it.\r\nChange the post to "Published" in order for regular users to read it', NULL, NULL, '2016-06-10 13:46:59', '2016-06-10 14:01:51', 0),
(11, 3, 'Kaffe?', 'Hur kan programmerare gÃ¶ra om kaffe till program? \r\nProgrammeraren dricker kaffe och spottar ut random ord med fingrarna och sÃ¥ hÃ¤nder magi! MAGI!', 'kaffedryck.jpg', '11', '2016-06-10 16:09:23', '2016-06-14 13:52:18', 1),
(17, 1, 'Lorem', 'There was a time... when hobbits existed.\r\nGollum was a weird dude.\r\nhttp://lotr.com is a site I do not know about.\r\nTesting all the hobbit functions now! =O', '004_lorem.jpg', '17', '2016-06-14 16:42:42', '2016-06-15 15:36:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `password` char(40) COLLATE utf8_swedish_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `group_id`, `created`, `modified`) VALUES
(1, 'admin', 'ded2d24ac521a206f4fa604e38557bde42a05c45', 1, '2016-06-09 10:17:30', '2016-06-09 10:17:30'),
(2, 'manager', '1e643e3142a2e38879cac9ee8765f66a3277b1f1', 2, '2016-06-09 10:17:46', '2016-06-09 10:17:46'),
(3, 'user', '8d6577df6866f7a8542f9419a66091b65102a6df', 3, '2016-06-09 10:17:55', '2016-06-09 10:17:55'),
(4, 'user2', '2c11a1716c48f99f7b936d83e71d4c3db58fee66', 3, '2016-06-10 14:02:06', '2016-06-13 08:07:07'),
(5, 'user3', 'd242762126a233778c923be1d93ffce570662386', 3, '2016-06-16 08:19:13', '2016-06-16 08:19:13');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
CREATE TABLE IF NOT EXISTS `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
  `part_no` varchar(12) COLLATE utf8_swedish_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

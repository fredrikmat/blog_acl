<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 * @property User $User
 */
class Post extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'published' => array(
			'numeric' => array(
				'rule' => array('lengthBetween', 0, 1),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'photo' => array(
			'phpSizeLimit' => array(
				'rule' => 'isUnderPhpSizeLimit',
				'message' => 'File exceeds upload filesize limit'
			),
			'formSizeLimit' => array(
				'rule' => 'isUnderFormSizeLimit',
        		'message' => 'File exceeds form upload filesize limit'
			),
			'completed' => array(
				'rule' => 'isCompletedUpload',
       		 	'message' => 'File was not successfully uploaded'
			),
			'tempDir' => array(
				'rule' => 'tempDirExists',
        		'message' => 'The system temporary directory is missing'
			),
			'writable' => array(
				'rule' => array('isWritable', false),
        		'message' => 'File upload directory was not writable'
			),
			'validExtension' => array(
				'rule' => array('isValidExtension', array('jpg', 'png', 'gif'), false),
				'message' => 'File does not have a pdf, png, or txt extension'	
			)
		)
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'post_id'
		)
	);

	public $actsAs = array(
        'Upload.Upload' => array(
            'photo' => array(
				'fields' => array(
					'dir' => 'photo_dir'
				)
            ),
			'thumbnailSizes' => array(
				'profile' => '160x160',
				'avatar' => '80x80',
				'thumb' => '40x40'
			),
			'thumbnailMethod' => 'php',
        )
    );

	public function isOwnedBy($post, $user) {
		return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
	}
}

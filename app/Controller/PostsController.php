<?php
App::uses('AppController', 'Controller');
App::uses('CakeTime', 'Utility');
/**
* Posts Controller
*
* @property Post $Post
* @property PaginatorComponent $Paginator
*/
class PostsController extends AppController {

	/**
	* Components
	*
	* @var array
	*/
	public $components = array(
		'Paginator',
		'Auth' => array(
            'authorize' => array('Controller')
        ),
	);

	/**
	* index method
	*
	* @return void
	*/
	public function index() {

		$availablePostIds = $this->Post->find('list', array('fields' => array('Post.id', 'Post.published')));
		foreach ($availablePostIds as $id => $published) {
			if (! $published && ! $this->isOwnerOrAdmin($id, 'controllers/Posts')) {
				unset($availablePostIds[$id]);
			}		
		}

		$this->Paginator->settings = array(
			'Post' => array(
				'limit' => 5,
				'order' => array('id' => 'desc'),
				'conditions' => array(
					'Post.id' => array_keys($availablePostIds)
				)
			)
		);

		$posts = $this->recursiveAddAdminAccess($this->Paginator->paginate());
		
		$this->Post->recursive = 0;
		$this->set('posts', $posts);
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index', 'view');

		$this->set('isAdmin', $this->isOwnerOrAdmin(-1, 'controllers/Posts'));


	}
	/**
	* view method
	*
	* @throws NotFoundException
	* @param string $id
	* @return void
	*/
	public function view($id = null) {

		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));		
		$post = $this->recursiveAddAdminAccess(array(0 => $this->Post->find('first', $options)))[0];

		if($post['Post']['published'] == 0) {
			if (! $post['Post']['adminAccess']) {
				throw new NotFoundException(__('Invalid post'));
			}
		}

		$this->set('post', $post);
	}

	/**
	* add method
	*
	* @return void
	*/
	public function add() {
		if ($this->request->is('post')) {			
			$this->Post->create();
			$this->request->data['Post']['user_id'] = $this->Auth->user('id'); // Set post owner to current user

			if (isset($this->request->data['Post']['preview'])) {
				$this->request->data['Post']['published'] = 0; // If Preview is pressed, the post should not be published
			}

			if ($this->Post->save($this->request->data)) {
				
				if (isset($this->request->data['Post']['preview'])) {
					$this->Flash->success(__('The post has been saved to draft.'));
					return $this->redirect(array('action' => 'view', $this->Post->id));
				} else {
					$this->Flash->success(__('The post has been published.'));
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Flash->error(__('The post could not be saved. Please, try again.'));
			}
		}
		$users = $this->Post->User->find('list');
		$this->set(compact('users'));
	}

	/**
	* edit method
	*
	* @throws NotFoundException
	* @param string $id
	* @return void
	*/
	public function edit($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->request->is(array('post', 'put'))) {

			if (isset($this->request->data['Post']['preview'])) {
				$this->request->data['Post']['published'] = 0; // If Preview is pressed, the post should not be published
			}
			
			if(isset($this->request->data['Post']['user_id']) && !$this->isOwnerOrAdmin(-1, 'controllers/Posts')) {
				unset($this->request->data['Post']['user_id']);
			}

			if ($this->Post->save($this->request->data)) {
				
				if (isset($this->request->data['Post']['preview'])) {
					$this->Flash->success(__('The post has been saved to draft.'));
					return $this->redirect(array('action' => 'view', $this->Post->id));
				} else {
					$this->Flash->success(__('The post has been published.'));
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Flash->error(__('The post could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			$this->request->data = $this->Post->find('first', $options);
		}
		$users = $this->Post->User->find('list', array(
				'fields' => array(
					'User.id', 
					'User.username'
				)
			)
		);
		$this->set(compact('users'));
	}

	/**
	* delete method
	*
	* @throws NotFoundException
	* @param string $id
	* @return void
	*/
	public function delete($id = null) {
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Post->delete()) {
			$this->Flash->success(__('The post has been deleted.'));
		} else {
			$this->Flash->error(__('The post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function isAuthorized($user)
	{	

		$group = array('model' => 'Group', 'foreign_key' => $user['group_id']);

		if (in_array($this->action, array('edit', 'delete'))) {
			$postId = (int) $this->request->params['pass'][0];
			
			if ($this->Post->isOwnedBy($postId, $user['id']) || $this->Acl->check($group, 'controllers/Posts', 'admin')) {
				return true;
			}
		}

		return false;
	}

	public function permission_check()
	{

		$group = array('model' => 'Group', 'foreign_key' => 3);

		//$this->Acl->allow($group, 'controllers/Posts/add'); // Deny/allow a permission
		//debug( $this->Acl->check($group, 'controllers/Posts/edit') );
		//$this->Acl->allow($group, 'controllers', 'admin');
		debug( $this->Acl->check($group, 'controllers/Posts/add') ); // custom field
		//exit;
	}

	private function isOwnerOrAdmin($postId, $acos)
	{
		if ($this->Auth->loggedIn()) {
			$group = array('model' => 'Group', 'foreign_key' => $this->Auth->user('group_id'));
			return $this->Post->isOwnedBy($postId, $this->Auth->user('id')) || $this->Acl->check($group, $acos, 'admin');
		}
		return false;
	}

	private function recursiveAddAdminAccess($array, $key = 0)
	{
		if($key == count($array)) {
			return $array;
		}
		
		$array[$key]['Post']['adminAccess'] = $this->isOwnerOrAdmin($array[$key]['Post']['id'], 'controllers/Posts');
		return $this->recursiveAddAdminAccess($array,  $key+1);
	}
}

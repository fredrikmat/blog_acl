<div class="posts form">
<?php echo $this->Form->create('Post', array('type' => 'file')); ?>
	<fieldset>
		<h1><?php echo __('Edit Post'); ?></h1>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('published');
		
		echo $this->Form->input('user_id', array('label' => 'Author', 'type' => !$isAdmin ? 'hidden' : 'select'));
		
		echo $this->Form->input('title');
		echo $this->Form->input('body');

		$imageLabel = "";
		if ($this->request->data['Post']['photo'] != null) {
			$imageLabel = ", current image: " . $this->request->data['Post']['photo'];
			echo $this->Form->input('Post.photo.remove', array('type' => 'checkbox', 'label' => 'Remove existing file'));
		}

		echo $this->Form->input('photo', array('type' => 'file', 'label' => 'Upload new image' . $imageLabel, 'required' => false));
		echo $this->Form->input('photo_dir', array('type' => 'hidden'));

	?>
	</fieldset>
<?php 
	echo $this->Form->submit('Preview', array('name' => 'Post[preview]', 'div' => array( 'style' => 'display:inline' ))); 
	$options = array(
		'label' => 'Save',
		'div' => array(
			'style' => 'display:inline'
		)
	);
	echo $this->Form->end($options); 
?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Post.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Post.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>

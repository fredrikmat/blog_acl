<div class="posts index">
	<h1><?php echo __('Posts'); ?></h1>
	
	<?php foreach ($posts as $post): 
	
	if (!$post['Post']['adminAccess'] && $post['Post']['published'] == 0) {
		continue; 
	}
	?>
	<div class="posts-multiview">
		<div class="post-single">
			<header>
				<h1><a href="<?= $this->Html->url( array('action' => 'view', $post['Post']['id']) ); ?>	"><?php echo h($post['Post']['title']); ?></a></h1>
				<div>
					<span><?php echo $this->Html->link($post['User']['username'], array('controller' => 'users', 'action' => 'view', $post['User']['id'])); ?></span>
					<span><?php echo CakeTime::niceShort($post['Post']['created']); ?></span>
					<?php if ($post['Post']['adminAccess']) : ?>
					<span><b><?php echo $post['Post']['published'] ? 'Published' : 'In draft'; ?></b></span>
					<?php endif; ?>
				</div>
			</header>
			<div class="content">
				<a href="<?= $this->Html->url( array('action' => 'view', $post['Post']['id']) ); ?>">
					<div class="image-placement">
						<?php if ($post['Post']['photo'] != null) {
							echo $this->Html->image('../files/post/photo/' . $post['Post']['photo_dir'] . '/' . $post['Post']['photo']); 
						} ?>
					</div>
				</a>
				<?php echo $this->Text->truncate( $this->Text->autoParagraph($this->Text->autoLinkUrls($post['Post']['body'])), 200, array('ellipsis' => '...', 'exact' => false, 'html')); ?>
			</div>
		</div>
	</div>
	<?php 
	/*
	
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('body'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
			<th><?php echo $this->Paginator->sort('published'); ?></th>
	</tr>
	</thead>
	<tbody>
	
	<tr>
		<td><?php echo h($post['Post']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($post['User']['username'], array('controller' => 'users', 'action' => 'view', $post['User']['id'])); ?>
		</td>
		<td><?php echo h($post['Post']['title']); ?>&nbsp;</td>
		<td><?php echo h($post['Post']['body']); ?>&nbsp;</td>
		<td><?php echo h($post['Post']['created']); ?>&nbsp;</td>
		<td><?php echo h($post['Post']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $post['Post']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $post['Post']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $post['Post']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $post['Post']['id']))); ?>
		</td>
		<td><?php echo $post['Post']['published'] ? 'Published' : 'In draft'; ?>&nbsp;</td>
	</tr>*/
	?>
<?php endforeach; ?>
	<!--/tbody>
	</table-->
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('New Post'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>

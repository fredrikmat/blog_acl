<div class="posts form">
<?php echo $this->Form->create('Post', array('type' => 'file')); ?>
	<fieldset>
		<h1><?php echo __('Add Post'); ?></h1>
	<?php
		// echo $this->Form->input('user_id');
		$sizes = array('1' => 'Published', '0' => 'Draft');
		echo $this->Form->input(
			'published',
			array('label' => 'Save as', 'options' => $sizes, 'default' => '1')
		);
		echo $this->Form->input('title');
		echo $this->Form->input('body');
		echo $this->Form->input('Post.photo', array('type' => 'file', 'required' => false));
		echo $this->Form->input('Post.photo_dir', array('type' => 'hidden'));
	?>
	</fieldset>
<?php
	echo $this->Form->submit('Preview', array('name' => 'Post[preview]', 'div' => array( 'style' => 'display:inline' ))); 
	$options = array(
		'label' => 'Save',
		'div' => array(
			'style' => 'display:inline'
		)
	);
	echo $this->Form->end($options); 
?>
</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>

		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="posts view">

	<?php /*
	<?php 
	if ($post['Post']['photo'] != null) {
		echo $this->Html->image('../files/post/photo/' . $post['Post']['photo_dir'] . '/' . $post['Post']['photo']); 
	}	
	?>
	
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($post['Post']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($post['User']['username'], array('controller' => 'users', 'action' => 'view', $post['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Published'); ?></dt>
		<dd>
			<?php echo $post['Post']['published'] ? "Published" : "In draft"; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($post['Post']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($post['Post']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($post['Post']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($post['Post']['modified']); ?>
			&nbsp;
		</dd>
	</dl>*/
	?>

	<div class="post-single">
		<header>
			<h1><?php echo h($post['Post']['title']); ?></h1>
			<div>
				<span><?php echo $this->Html->link($post['User']['username'], array('controller' => 'users', 'action' => 'view', $post['User']['id'])); ?></span>
				<span><?php echo CakeTime::niceShort($post['Post']['created']); ?></span>
				<span><?php $count = count($post['Comment']); echo $count ? $count==1 ? $count . " comment" :  $count . " comments" : "no comments";   ?></span>
				<?php if ($post['Post']['adminAccess']) : ?>
				<span><b><?php echo $post['Post']['published'] ? 'Published' : 'In draft'; ?></b></span>
				<?php endif; ?>
			</div>
		</header>
		<div class="content">
			<div class="image-placement">
				<?php 
					if ($post['Post']['photo'] != null) {
						echo $this->Html->image('../files/post/photo/' . $post['Post']['photo_dir'] . '/' . $post['Post']['photo']); 
					}	
				?>
			</div>
			<?php echo $this->Text->autoParagraph($this->Text->autoLinkUrls($post['Post']['body'])); ?>
		</div>
	</div>


	<br/>
	<h2><?= __('Comments');?></h2>
	
	<?php foreach ($post['Comment'] as $comment) : ?>
	
	<div class="comment">
		<div class="profile">
			<span><?= h($comment['name']) ?></span>
			<?= $this->Gravatar->image($comment['email'], array('size' => 80)); ?>
		</div>
		<div class="content">
			<span><?= CakeTime::niceShort($comment['created']); ?></span>
			<p><?= $this->Text->autoLinkUrls(h($comment['body'])); ?></p>
		</div>
	</div>

	<?php endforeach; ?>

	
	<?php echo $this->Form->create('Comment', array('url' => array('controller' => 'comments', 'action' => 'add'))); ?>
		<fieldset>
		<?php
			echo $this->Form->input('post_id', array('type' => 'hidden', 'value' => $post['Post']['id']));
			echo $this->Form->input('name', array('div' => array('style' => 'display: inline-block;')));
			echo $this->Form->input('email', array('div' => array('style' => 'display: inline-block;')));
			echo $this->Form->input('body');
		?>
		</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
	

</div>
<div class="actions">
	<h1><?php echo __('Actions'); ?></h1>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Post'), array('action' => 'edit', $post['Post']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Post'), array('action' => 'delete', $post['Post']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $post['Post']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
